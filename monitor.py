import json
import os
import subprocess, platform
import logging
import math  
import requests 
import datetime

# calculate percent of used space out of specified max size
def PercentageOfGivenMaxSpace(pathStr,maxSize):
    sizeSum=0
    cDir=os.getcwd()
    fsize=1
    os.chdir(pathStr)
    for path, dirs, files in os.walk(pathStr):
        for f in files:
            fp = os.path.join(path, f)
            sizeSum += os.path.getsize(fp)

    if (sizeSum > 1024 * 1024):
            fsize = math.ceil(sizeSum / (1024 * 1024))  
    os.chdir(cDir)
    return (fsize/maxSize)*100

# Ping URL A and report status
def PingStatus(url,waitInSec):    
    try:
        cmd = "ping -{} {} {}".format('n' if platform.system().lower()=="windows" else 'c',waitInSec, url, shell=True)
        output = subprocess.check_output(cmd).decode().strip()
        for line in output.split("\n"):
            if("Minimum" in line):
                timing = line.split(',')
            elif("Lost" in line):
                packets = line.split(',')                
        return {
            'type': 'RTT',
            'min': timing[0],
            'avg': timing[1],
            'max': timing[2],        
            'loss': packets[2].split()[2],
            'msg':'Ping Successful'
        }        
    except Exception as e:
        logging.error("Status: Error, "+ str(e))
        return {
            'type': 'RTT',
            'msg':'Ping Failed',
            'reason':str(e)
         }
        
# Write to url B
def WriteToUrl(urlB,PARAMS):
    try:
        r = requests.get(url = urlB, params = PARAMS) 
        if r:
            print('Success')
    except Exception as e:
		return str(e)
        
# Start Of Main Program
# This Program reads from Config.json 
if __name__ == "__main__":
    with open('config.json') as jsonfile:
        # config values are read into configuredData
        configuredData=json.load(jsonfile)
        url_a=""
        url_b=""
        waitForPingInSeconds=1
        logFolderWithFile=""
        maxLogFolderSize=0
        # From configuredData json string, resource names/ values are read
        # Read Server Info for pinging 
        for srv in configuredData['servers']:
            if(srv['direction']=="from"):
                url_a=srv['url']
                waitForPingInSeconds=int(srv['wait_time'])
            elif(srv['direction']=="to"):
                url_b=srv['url']
        jsonLogKeyValues=configuredData['Logger']
        # Read logging info
        for k,v in jsonLogKeyValues.iteritems():           
            if('max-size-folder-in-mb' in k):
                maxLogFolderSize=int(v)
                continue
            elif('file_name' in k):
                logFile=v
                continue
            elif('folder_path' in k):
                folder_path=v
                continue
        if(logFile!="" and folder_path!=""):            
            if(len(folder_path)>0):
               spaceUsedInPercent=  PercentageOfGivenMaxSpace(folder_path,maxLogFolderSize)    
        
        #set directory for logging       
        os.chdir(folder_path)
        logFile=str(datetime.datetime.now()).replace(" ", "").replace('.','_')+''                                           
        logFile=logFile.replace('-','_').replace(':','_')
        os.mkdir(logFile)
        os.chdir(logFile)
        logging.basicConfig(filename="monitor.log",level=logging.DEBUG,filemode='w')
        
             #ping url_a
        status=PingStatus(url_a,int(waitForPingInSeconds))
        
        # Report Status
        if("Error" in status):
            logging.error(status)
        else:
            logging.info(status)
        
                # Report Space used till date
        if(spaceUsedInPercent>80):
            logging.critical("{} % Space Used of Specified Size of {} MB".format(str(spaceUsedInPercent),maxLogFolderSize))
        else:
            logging.info("{} % Space Used of Specified Size of {} MB".format(str(spaceUsedInPercent),maxLogFolderSize))
        # Write to URL
        WriteToUrl(url_b,status)
# End of Monitor program