import pydicom as dicom
import matplotlib.pyplot as plt
import os
import cv2
import PIL # optional
import pandas as pd
import csv
import urllib3
import io
import requests
import matplotlib.pyplot as plt
# make it True if you want in PNG format
PNG = False
# Specify the .dcm folder path

folder_path = "C:\\Python\\AsssignmentOne\\dicomImages"

# Specify the .jpg/.png folder path
jpg_folder_path ="C:\\Python\\AsssignmentOne\\JPEG"
images_path = os.listdir(folder_path)

url="https://raw.githubusercontent.com/vivek8981/DICOM-to-JPG/master/dicom_image_description.csv"
s=requests.get(url).content
dicom_image_description=pd.read_csv(io.StringIO(s.decode('utf-8')))

 
with open('C:\\Python\\AsssignmentOne\\csv\\Patient_Detail.csv', 'w', newline ='') as csvfile:
    fieldnames = list(dicom_image_description["Description"])
    fieldnames.append("InstitutionName")
    dfDicomProps = pd.DataFrame(columns=fieldnames)
    writer = csv.writer(csvfile, delimiter=',')
    writer.writerow(fieldnames)
    for n, image in enumerate(images_path):        
        ds = dicom.dcmread(os.path.join(folder_path, image))
        #print(ds.data_element)
        rows = []
        for field in fieldnames:
            try:
                if(field!="PixelData"):
                    x = str(ds.data_element(field)).replace("'", "")
                    y = x.find(":")
                    x = x[y+2:]
                    rows.append(x)
                else:
                    rows.append('')

            except:
                rows.append('')
            # Write Record to Excel File
            writer.writerow(rows)
           
dicom_Table=pd.read_csv("C:\\Python\\AsssignmentOne\\csv\\Patient_Detail.csv")
ValidPatientInfo=dicom_Table[dicom_Table.PatientName.notnull()] 
ValidPatientInfo=ValidPatientInfo[dicom_Table.PatientSex.notnull()] 
ValidPatientInfo=ValidPatientInfo[dicom_Table.PatientAge.notnull()] 
ValidPatientInfo=ValidPatientInfo[dicom_Table.InstitutionName.notnull()] 
ValidPatientInfo=ValidPatientInfo[['PatientName','PatientSex','PatientAge','InstitutionName','StudyInstanceUID','SeriesInstanceUID']]
ValidPatientInfo.to_csv("C:\\Python\\AsssignmentOne\\csv\\FilteredPatientData.csv", sep=',', encoding='utf-8', index=False)
